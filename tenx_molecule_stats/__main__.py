#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" tenx_molecule_stats

Generate moecule level statistics from 10x Genomics aligned bam file
"""

import argparse
import logging
import sys
import pysam

__version__ = "0.1"
__author__ = "Lance Parsons"
__author_email__ = "lparsons@princeton.edu"
__copyright__ = "Copyright 2018, Lance Parsons"
__license__ = "MIT https://opensource.org/licenses/MIT"


class Molecule():
    def __init__(self, alignment):
        self.molecule_id = alignment.get_tag('MI')
        self.droplet_id = alignment.get_tag('BX')
        self.reference_name = alignment.reference_name
        self.start = alignment.reference_start
        self.end = alignment.reference_end
        self.num_reads = 1
        self.non_duplicate_reads = 0
        self.duplicate_reads = 0
        if alignment.is_duplicate:
            self.duplicate_reads += 1
        else:
            self.non_duplicate_reads += 1

    def add_read(self, alignment):
        if self.droplet_id != alignment.get_tag('BX'):
            raise(RuntimeError("Droplet IDs don't match"))
        if self.reference_name != alignment.reference_name:
            self.reference_name = None
            self.start = None
            self.end = None
        else:
            self.start = min(self.start, alignment.reference_start)
            self.end = max(self.end, alignment.reference_end)
        self.num_reads += 1
        if alignment.is_duplicate:
            self.duplicate_reads += 1
        else:
            self.non_duplicate_reads += 1

    def __repr__(self):
        return("Molecule {}".format(self.molecule_id))


def main():

    parser = argparse.ArgumentParser(
        description="Generate moecule level statistics from 10x Genomics "
        "aligned bam file",
        epilog="As an alternative to the commandline, params can be placed in "
        "a file, one per line, and specified on the commandline like "
        "'%(prog)s @params.conf'.",
        fromfile_prefix_chars='@')
    # TODO Specify your real parameters here.
    parser.add_argument("filename",
                        help="10x posistion sorted bam filename",
                        metavar="FILE")
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)

    logging.debug("Parsing {}".format(args.filename))

    bamfile = pysam.AlignmentFile(args.filename, "rb")
    alignments = bamfile.fetch()
    molecules = dict()
    for i, alignment in enumerate(alignments):
        if i > 0 and i % 100000 == 0:
            sys.stderr.write("Read {} alignments\n".format(i))
        try:
            molecule_id = alignment.get_tag('MI')
        except KeyError:
            continue
        molecule = molecules.get(molecule_id)
        if (molecule):
            logging.debug("Adding alignment {} to {}".format(
                alignment, molecule))
            molecule.add_read(alignment)
        else:
            molecule = Molecule(alignment)
            molecules[molecule_id] = molecule
            logging.debug("Added new molecule: {}".format(molecule))

    print("molecule_id\treference_name\treference_start\treference_end\t"
          "droplet_id\tnum_reads\tnon_duplicate_reads\tduplicate_reads")
    for molecule_id in sorted(molecules.keys()):
        molecule = molecules.get(molecule_id)
        print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(
            molecule.molecule_id, molecule.reference_name, molecule.start,
            molecule.end, molecule.droplet_id, molecule.num_reads,
            molecule.non_duplicate_reads, molecule.duplicate_reads))

    sys.stderr.write("Processed {} alignments, complete.\n".format(i))


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
