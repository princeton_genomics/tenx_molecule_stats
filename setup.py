#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup

setup(
    name='tenx_molecule_stats',
    version='0.1.0',
    packages=['tenx_molecule_stats'],
    entry_points={
        'console_scripts': [
            'tenx_molecule_stats=tenx_molecule_stats.__main__:main'
        ]
    })
